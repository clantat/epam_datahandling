package com.clantat;

import java.math.BigDecimal;

public class Other {
    public BigDecimal getCircleSquare(double radius){
        return new BigDecimal(radius).pow(2).multiply(new BigDecimal(Math.PI)).setScale(50,BigDecimal.ROUND_HALF_UP);
    }

    public boolean compare(String TheFirst, String TheSecond, String TheThird) {
        BigDecimal first = new BigDecimal(TheFirst);
        BigDecimal second = new BigDecimal(TheSecond);
        BigDecimal third = new BigDecimal(TheThird);
        return first.add(second).compareTo(third) == 0;
    }
    public double min(double TheFirst, double TheSecond, double TheThird) {
        return Math.min(TheFirst, Math.min(TheSecond, TheThird));
    }

    public double max(double TheFirst, double TheSecond, double TheThird) {
        return Math.max(TheFirst, Math.max(TheSecond, TheThird));
    }
}
