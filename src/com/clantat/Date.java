package com.clantat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Date {
    public void myAge(LocalDateTime Birth){
        LocalDateTime today = LocalDateTime.now();
        System.out.printf("Ваш возраст с момента рождения: %d лет, %d месяцев, %d дней, %d часов, %d минут, %d секунд\n",
                ChronoUnit.YEARS.between(Birth, today),
                ChronoUnit.MONTHS.between(Birth, today),
                ChronoUnit.DAYS.between(Birth, today),
                ChronoUnit.HOURS.between(Birth, today),
                ChronoUnit.MINUTES.between(Birth, today),
                ChronoUnit.SECONDS.between(Birth, today));
    }

    public long twoDays(String TheFirstDate, String TheSecondDate) {
        DateTimeFormatter format =  DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate firstDate = LocalDate.parse(TheFirstDate, format);
        LocalDate secondDate = LocalDate.parse(TheSecondDate, format);
        return  ChronoUnit.DAYS.between(firstDate, secondDate);
    }

    public String converting (String date) {
        ZonedDateTime zonedDate = ZonedDateTime.parse(date).withZoneSameInstant(ZoneId.of("Europe/Samara"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return zonedDate.format(formatter);
    }
}
