package com.clantat;
import java.time.LocalDateTime;

public class Demo {


    public void start() {
        //Date
        Date date = new Date();
        System.out.println("1.1"); //1.1
        date.myAge(LocalDateTime.of(1998, 7, 6, 12, 45, 33));

        System.out.println("1.2"); //1.2
        String TheFirstDate = "25.07.1921";
        String TheSecondDate = "11.09.2001";
        System.out.printf("Разница между датой %s и датой %s равна: %d дней \n",
                TheFirstDate, TheSecondDate, date.twoDays(TheFirstDate, TheSecondDate));

        System.out.println("1.3"); //1.3
        String DateOfFight = "2016-08-16T10:15:30+08:00";
        System.out.printf("From %s time \nto Izhevsk %s time \n",
                DateOfFight, date.converting(DateOfFight));

        //Other
        Other other = new Other();
        System.out.println("2.1"); //2.1
        double radius = 2.28;
        System.out.printf("Площадь круга с радиусом %s равна %s\n", radius, other.getCircleSquare(radius));

        System.out.println("2.2"); //2.2
        String TheFirst = "0.1";
        String TheSecond = "0.15";
        String TheThird = "0.25";
        System.out.printf("Даны три числа: %s, %s и %s \n", TheFirst, TheSecond, TheThird);
        if (other.compare(TheFirst, TheSecond, TheThird))
            System.out.println("Третье число является суммой двух первых");
        else
            System.out.println("Третье число является суммой двух первых");

        System.out.println("2.3"); //2.3
        double i1 = -322.223;
        double i2 = 228.822;
        double i3 = 1488.8841;
        System.out.printf("Даны три числа: %.3f, %.3f и %.4f.\nИз них минимум: %.4f ;\nМаксимум: %.4f\n",
                i1, i2, i3,
                other.min(i1, i2, i3),
                other.max(i1, i2, i3));

    }
}
